package com.example.twitter.screens.list.di

import com.example.twitter.api.ApiInterface
import com.example.twitter.screens.list.TweetViewModel
import com.example.twitter.screens.list.data.ListRepository
import com.example.twitter.screens.list.data.remote.ListRemoteDataSource
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val listViewModel = module {
    viewModel {
        TweetViewModel(get(), get())
    }
}

val listRepositoryModule = module {
    fun provideListRepository(remoteDataSource: ListRemoteDataSource): ListRepository {
        return ListRepository(remoteDataSource)
    }
    factory { provideListRepository(get()) }
}

val ListDataSourceModule = module {
    fun provideListDataSourceModule(api: ApiInterface): ListRemoteDataSource {
        return ListRemoteDataSource(api)
    }
    single { provideListDataSourceModule(get()) }
}