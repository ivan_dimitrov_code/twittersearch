package com.example.twitter.screens.list.models.server

data class ServerTweetListModel(
    val statuses: List<ServerTweetInfo>
)