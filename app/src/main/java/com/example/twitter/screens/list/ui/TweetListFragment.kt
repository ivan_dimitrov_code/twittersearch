package com.example.twitter.screens.list.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitter.screens.list.TweetViewModel
import com.example.twitter.R
import com.example.twitter.screens.list.models.app.TweetInfo
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class TweetListFragment : Fragment(), TweetListAdapter.OnExpandButtonClickListener {
    private val viewModel by viewModel<TweetViewModel>()
    private val adapter: TweetListAdapter = TweetListAdapter(mutableListOf(), this)
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tweetListRecyclerView.addItemDecoration(
            DividerItemDecoration(
                tweetListRecyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )

        tweetListRecyclerView.layoutManager = LinearLayoutManager(context)
        tweetListRecyclerView.adapter = adapter
        startLiveDataObservation()
        val query = arguments?.getString(SEARCH_QUERY_CODE)
        query?.let { viewModel.requestData(it) }
    }

    private fun startLiveDataObservation() {
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            it?.let { state ->
                when (state) {
                    is SearchState -> enterSearchState()
                    is LoadedItemsState -> enterLoadedItemsState(state.listData)
                    is LoadingState -> enterLoadingState()
                    is ErrorState -> listener?.onError()
                    is ToggleCellState -> enterToggleState(state)
                }
            }
        })
    }

    private fun enterToggleState(state: ToggleCellState) {
        adapter.updateCellState(state.position)
        tweetListRecyclerView.visibility = View.VISIBLE
        tweetListProgressBar.visibility = View.INVISIBLE
    }

    private fun enterLoadingState() {
        tweetListRecyclerView.visibility = View.INVISIBLE
        tweetListProgressBar.visibility = View.VISIBLE
    }

    private fun enterLoadedItemsState(list: List<TweetInfo>) {
        adapter.injectList(list)
        tweetListRecyclerView.visibility = View.VISIBLE
        tweetListProgressBar.visibility = View.INVISIBLE
    }

    private fun enterSearchState() {
        tweetListRecyclerView.visibility = View.INVISIBLE
        tweetListProgressBar.visibility = View.VISIBLE
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    interface OnFragmentInteractionListener {
        fun onError()
    }

    companion object {
        const val SEARCH_QUERY_CODE = "query_code"
        const val TAG = "TweetListFragment"
        fun newInstance(bundle: Bundle?) = TweetListFragment().apply {
            this.arguments = bundle
        }
    }

    override fun onCellClick(tweetInfo: TweetInfo, position: Int) {
        viewModel.onCellClick(tweetInfo, position)
    }
}
