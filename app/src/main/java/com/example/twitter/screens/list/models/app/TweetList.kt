package com.example.twitter.screens.list.models.app

data class TweetList(
    val statuses: List<TweetInfo>
)