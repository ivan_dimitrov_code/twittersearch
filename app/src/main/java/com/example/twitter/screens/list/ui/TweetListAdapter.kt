package com.example.twitter.screens.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.twitter.R
import com.example.twitter.screens.list.models.app.TweetInfo
import kotlinx.android.synthetic.main.cell_tweet_list.view.*


const val MAX_CELL_LINES = 20
const val MIN_CELL_LINES = 1

class TweetListAdapter(
    private val list: MutableList<TweetInfo>,
    private val listener: OnExpandButtonClickListener
) : RecyclerView.Adapter<TweetListAdapter.ViewHolder>() {

    private val displayedList: MutableList<TweetInfo> = mutableListOf()

    fun updateCellState(position: Int) {
        notifyItemChanged(position)
    }

    fun injectList(newList: List<TweetInfo>) {
        list.clear()
        list.addAll(newList)
        displayedList.clear()
        displayedList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_tweet_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayedList[position]
        setupExpandDependendItems(item, holder)
        holder.view.setOnClickListener {
            listener.onCellClick(item, position)
        }
        holder.expandButton.setOnClickListener {
            listener.onCellClick(item, position)
        }
        holder.tweetNumber.text = "${(position + 1)}."
        holder.tweetText.text = item.text
    }

    private fun setupExpandDependendItems(item: TweetInfo, holder: ViewHolder) {
        if (item.isExpanded) {
            holder.expandButton.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
            holder.tweetText.maxLines = MAX_CELL_LINES
        } else {
            holder.expandButton.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            holder.tweetText.maxLines = MIN_CELL_LINES
        }
    }

    interface OnExpandButtonClickListener {
        fun onCellClick(tweetInfo: TweetInfo, position: Int)
    }

    override fun getItemCount(): Int = displayedList.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tweetText: TextView = view.tweetText
        val tweetNumber: TextView = view.tweetNumber
        val expandButton: Button = view.tweetExpandButton
    }

}
