package com.example.twitter.screens.list.models.server

data class ServerTweetInfo(
    val text: String
)