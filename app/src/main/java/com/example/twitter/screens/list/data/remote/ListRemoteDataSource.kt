package com.example.twitter.screens.list.data.remote

import com.example.twitter.api.ApiInterface
import com.example.twitter.screens.list.models.app.TweetInfo
import com.example.twitter.screens.list.models.server.ServerTweetListModel
import com.google.gson.Gson
import io.reactivex.Single

class ListRemoteDataSource(private val apiClient: ApiInterface) {

    fun requestTweetList(searchQuery: String): Single<List<TweetInfo>> {

        val gson = Gson()
        val dummyData = gson.fromJson<ServerTweetListModel>(
            getDummyTweetData(),
            ServerTweetListModel::class.java
        )
        return Single.just(dummyData).map {
            adaptServerToAppModel(it).statuses
        }

//        return apiClient.requestData().map {
//            adaptServerToAppModel(it).statuses
//        }
    }
}
