package com.example.twitter.screens.list.data

import com.example.twitter.screens.list.data.remote.ListRemoteDataSource
import com.example.twitter.screens.list.models.app.TweetInfo
import io.reactivex.Single

open class ListRepository(private val remoteDataSource: ListRemoteDataSource) {

    private var cachedList: MutableList<TweetInfo>? = null

    fun getListData(searchQuery: String): Single<List<TweetInfo>> {
        return if (cachedList == null) {
            remoteDataSource.requestTweetList(searchQuery).map {
                cachedList = it.toMutableList()
                cachedList
            }
        } else {
            Single.just(cachedList)
        }
    }

    fun clearCache() {
        cachedList?.clear()
    }

    fun toggleExpandStatusForCell(tweetInfo: TweetInfo) {
        val tweet = cachedList?.find { it == tweetInfo }
        tweet?.let { tweet.isExpanded = (!tweet.isExpanded) }
    }
}