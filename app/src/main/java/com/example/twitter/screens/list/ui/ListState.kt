package com.example.twitter.screens.list.ui

import com.example.twitter.screens.list.models.app.TweetInfo

sealed class ListState {
    abstract val loadedAllItems: Boolean
}

data class SearchState(override val loadedAllItems: Boolean) : ListState()
data class ToggleCellState(override val loadedAllItems: Boolean, val position: Int) : ListState()

data class LoadedItemsState(
    override val loadedAllItems: Boolean,
    val listData: List<TweetInfo>
) : ListState()

data class LoadingState(override val loadedAllItems: Boolean) : ListState()
data class ErrorState(val errorMessage: String, override val loadedAllItems: Boolean) : ListState()
