package com.example.twitter.screens.list.models.app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TweetInfo(
    val text: String,
    var isExpanded: Boolean
) : Parcelable