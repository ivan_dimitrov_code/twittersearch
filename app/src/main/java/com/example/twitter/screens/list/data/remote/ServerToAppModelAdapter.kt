package com.example.twitter.screens.list.data.remote

import com.example.twitter.screens.list.models.app.TweetInfo
import com.example.twitter.screens.list.models.app.TweetList
import com.example.twitter.screens.list.models.server.ServerTweetListModel

fun adaptServerToAppModel(serverModelList: ServerTweetListModel): TweetList {
    val mapedData =  serverModelList.statuses.map {
        TweetInfo(it.text,false)
    }

    return TweetList(mapedData)
}