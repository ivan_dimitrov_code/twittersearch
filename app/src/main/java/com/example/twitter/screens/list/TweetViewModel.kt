package com.example.twitter.screens.list

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.example.twitter.screens.list.data.ListRepository
import com.example.twitter.screens.list.models.app.TweetInfo
import com.example.twitter.screens.list.ui.*
import com.example.twitter.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class TweetViewModel(
    private val repository: ListRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) : ViewModel() {
    val uiState = MutableLiveData<ListState>()
    private val disposable = CompositeDisposable()

    fun requestData(searchQuery: String) {
        uiState.value = LoadingState(false)
        val requestDisposable = repository.getListData(searchQuery)
            .observeOn(baseSchedulerProvider.ui())
            .subscribeOn(baseSchedulerProvider.io())
            .subscribe({
                uiState.value = LoadedItemsState(true, it)
            }, {
                uiState.value = it.message?.let { errorMessage ->
                    ErrorState(errorMessage, false)
                }
            })
        disposable.add(requestDisposable)
    }

    fun onCellClick(tweetInfo: TweetInfo, position: Int) {
        repository.toggleExpandStatusForCell(tweetInfo)
        uiState.value = ToggleCellState(true, position)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun clearDisposable() {
        disposable.clear()
    }
}
