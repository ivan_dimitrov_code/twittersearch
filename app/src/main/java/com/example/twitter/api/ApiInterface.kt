package com.example.twitter.api

import com.example.twitter.screens.list.models.server.ServerTweetListModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("1.1/search/tweets.json")
    fun requestData(): Single<ServerTweetListModel>
}