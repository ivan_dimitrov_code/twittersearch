package com.example.twitter.utils.schedulers

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class ImmediateSchedulerProvider : BaseSchedulerProvider {

    override fun computation(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun ui(): Scheduler {
        return Schedulers.trampoline()
    }

    companion object {

        private var INSTANCE: ImmediateSchedulerProvider? = null

        val instance: ImmediateSchedulerProvider
            @Synchronized get() {
                if (INSTANCE == null) {
                    INSTANCE = ImmediateSchedulerProvider()
                }
                return INSTANCE!!
            }
    }
}
