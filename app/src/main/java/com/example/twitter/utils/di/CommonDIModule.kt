package com.example.twitter.utils.di

import com.example.twitter.api.ApiClient
import com.example.twitter.utils.schedulers.BaseSchedulerProvider
import com.example.twitter.utils.schedulers.ImmediateSchedulerProvider
import com.example.twitter.utils.schedulers.SchedulerProvider
import org.koin.dsl.module

val apiClient = module {
    single { ApiClient.getClient() }
}

val rxScheduleProvider = module {
    fun provideScheduleProvider(): BaseSchedulerProvider {
        return SchedulerProvider.instance
    }
    single { provideScheduleProvider() }
}

val rxTestSchedulerProvider = module {
    fun provideScheduleProvider(): BaseSchedulerProvider {
        return ImmediateSchedulerProvider.instance
    }
    single { provideScheduleProvider() }
}