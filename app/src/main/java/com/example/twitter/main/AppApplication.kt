package com.example.twitter.main

import android.app.Application
import com.example.twitter.screens.list.di.ListDataSourceModule
import com.example.twitter.screens.list.di.listRepositoryModule
import com.example.twitter.screens.list.di.listViewModel
import com.example.twitter.utils.di.apiClient
import com.example.twitter.utils.di.rxScheduleProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    rxScheduleProvider,
                    apiClient,
                    listViewModel,
                    listRepositoryModule,
                    ListDataSourceModule
                )
            )
        }
    }
}