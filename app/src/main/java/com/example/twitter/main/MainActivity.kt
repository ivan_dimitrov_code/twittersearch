package com.example.twitter.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.twitter.R
import com.example.twitter.screens.error.ErrorFragment
import com.example.twitter.screens.list.ui.TweetListFragment
import com.example.twitter.screens.search.SearchFragment
import com.example.twitter.utils.NetworkState

const val LAST_FRAGMENT_NAME_KEY = "LAST_FRAGMENT_NAME_KEY"

class MainActivity : AppCompatActivity(), TweetListFragment.OnFragmentInteractionListener,
    ErrorFragment.OnFragmentInteractionListener, SearchFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            if (NetworkState.isConnectedToNetwork(this)) {
                showSearchScreen()
            } else {
                showErrorScreen()
            }
        } else {
            val fragmentTag = savedInstanceState.getString(LAST_FRAGMENT_NAME_KEY)
            fragmentTag?.let { showFragmentWithTag(it) }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(LAST_FRAGMENT_NAME_KEY, getCurrentFragment()?.tag)
        super.onSaveInstanceState(outState)
    }

    private fun showSearchScreen(){
        showFragmentWithTag(SearchFragment.TAG)
    }

    private fun showErrorScreen() {
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        showFragmentWithTag(ErrorFragment.TAG)
    }

    private fun showListScreen(bundle: Bundle?) {
        showFragmentWithTag(TweetListFragment.TAG, bundle)
    }

    private fun showFragmentWithTag(tag: String, bundle: Bundle? = null) {
        val fragment = provideFragmentForTag(tag, bundle)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mainFragmentContainer, fragment!!, tag)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun provideFragmentForTag(tag: String, bundle: Bundle? = null): Fragment? {
        return if (supportFragmentManager.findFragmentByTag(tag) != null) {
            supportFragmentManager.findFragmentByTag(tag)
        } else {
            when (tag) {
                TweetListFragment.TAG -> TweetListFragment.newInstance(bundle)
                ErrorFragment.TAG -> ErrorFragment.newInstance()
                SearchFragment.TAG -> SearchFragment.newInstance()
                else -> Fragment()
            }
        }
    }

    override fun onBackPressed() {
        if (getCurrentFragment()?.tag == TweetListFragment.TAG) {
            supportFragmentManager.popBackStackImmediate()
            return
        }

        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        }
    }

    override fun onError() {
        showErrorScreen()
    }

    private fun getCurrentFragment(): Fragment? {
        return if (supportFragmentManager.fragments.size > 0) {
            supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1]
        } else {
            null
        }
    }

    override fun onRetryClick() {
        if (NetworkState.isConnectedToNetwork(this)) {
            showSearchScreen()
        }
    }

    override fun onSearchPressed(query: String) {
        val bundle = Bundle()
        bundle.putString(TweetListFragment.SEARCH_QUERY_CODE, query)
        showListScreen(bundle)
    }
}
